package es.vencinas.sngular.rest.client;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import es.vencinas.sngular.exception.SslException;

/**
 * The Interface RestClient.
 */
public interface RestClient {

  /**
   * Gets the request factory.
   *
   * @return the request factory
   * @throws SslException
   *             the ssl exception
   */
  HttpComponentsClientHttpRequestFactory getRequestFactory() throws SslException;

}
