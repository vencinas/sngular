package es.vencinas.sngular.exception;

import javax.ws.rs.core.Response.Status.Family;

/**
 * The Class NotFountSngularException.
 */
public class NotFountSngularException extends SngularException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7253382327197648347L;

    /** The Constant STATUS_CODE. */
    private static final int STATUS_CODE = 17;

    /**
     * Instantiates a new not fount sngular exception.
     *
     * @param reasonPhrase
     *            the reason phrase
     */
    public NotFountSngularException(String reasonPhrase) {
        super(Family.OTHER, NotFountSngularException.STATUS_CODE, reasonPhrase);
    }

}
