package es.vencinas.sngular.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.vencinas.sngular.domain.CurrencyRate;
import es.vencinas.sngular.exception.NotFountSngularException;
import es.vencinas.sngular.exception.SngularException;
import es.vencinas.sngular.service.CurrencyRateFacade;

/**
 * REST layer for managing currency rate.
 *
 * @author Valentín Encinas
 */
@RestController
@RequestMapping("/api/currencyRate/v1/")
public class CurrencyRateController {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyRateController.class);

    /**
     * Currency Rate Facade
     */
    @Autowired
    private CurrencyRateFacade currencyRateFacade;

    /**
     * Returns all resources (CurrencyRate) and a HTTP Status of 200 OK
     *
     * @return
     */
    // @Compress //can be used only if you want to SELECTIVELY enable compression at the method level.
    // By using the EncodingFilter everything is compressed now.
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Map<String, List<CurrencyRate>>> getAllCurrencyRates() {
        CurrencyRateController.LOGGER.info("[IN]\tCurrencyRateService:getAllCurrencyRates()");

        ResponseEntity<Map<String, List<CurrencyRate>>> response;

        final Map<String, List<CurrencyRate>> currencyRates = currencyRateFacade.getAllCurrencyRates();
        response = new ResponseEntity<>(currencyRates, HttpStatus.OK);

        CurrencyRateController.LOGGER.info("[OUT]\tCurrencyRateService:getAllCurrencyRates()\t\t[OK]");
        return response;
    }

    /************************************ READ ************************************/

    /**
     *
     * @param sourceCountry
     * @param targetCountries
     *
     * @return
     */
    @RequestMapping(value = "/{sourceCountry}/{targetCountry}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Map<String, List<CurrencyRate>>> getCurrencyRates(@PathVariable("sourceCountry") String sourceCountry, @PathVariable("targetCountry") List<String> targetCountries) {
        Assert.notNull(sourceCountry);
        Assert.notNull(targetCountries);
        CurrencyRateController.LOGGER.info("[IN]\t\tCurrencyRateService:getCurrencyRates(" + sourceCountry + "," + targetCountries + ")");

        ResponseEntity<Map<String, List<CurrencyRate>>> response = null;
        try {
            Map<String, List<CurrencyRate>> currenciesRateCountry = currencyRateFacade.getCurrenciesRateCountry(sourceCountry, targetCountries);
            response = new ResponseEntity<>(currenciesRateCountry, HttpStatus.OK);
        } catch (NotFountSngularException exception) {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        CurrencyRateController.LOGGER.info("[OUT]\tCurrencyRateService:getCurrencyRates()\t\t[OK]");
        return response;
    }

    // --- Error handlers

    /**
     *
     * @param exception
     *
     * @return
     */
    @ExceptionHandler(NotFountSngularException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public String handleCurrencyRateNotFoundExrateeption(SngularException exception) {
        CurrencyRateController.LOGGER.error("[IN]\tCurrencyRateService:handleCurrencyRateNotFoundExrateeption()");
        return exception.getMessage();
    }

}
