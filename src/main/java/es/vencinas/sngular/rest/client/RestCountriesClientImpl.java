package es.vencinas.sngular.rest.client;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.vencinas.sngular.domain.Country;
import es.vencinas.sngular.exception.JSonExcepcion;
import es.vencinas.sngular.exception.SslException;
import es.vencinas.sngular.service.CurrencyRateFacadeImpl;

/**
 * The Class RestCountriesClientImpl.
 */
@Service
public class RestCountriesClientImpl extends RestClientImpl implements RestCountriesClient {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyRateFacadeImpl.class);

    @Override
    public List<Country> getAllCountries() throws SslException {

        ResponseEntity<String> response = null;
        final RestTemplate restTemplate = new RestTemplate(getRequestFactory());
        response = restTemplate.exchange(RestCountriesClient.URL_GET_ALL_COUNTRIES, HttpMethod.GET, null, String.class);

        List<Country> countries = null;
        if (response.getStatusCode() == HttpStatus.OK) {
            final ObjectMapper mapper = new ObjectMapper();

            // JSON from String to Object
            try {
                countries = mapper.readValue(response.getBody(), mapper.getTypeFactory().constructCollectionType(List.class, Country.class));
            } catch (final IOException exception) {
                countries = null;
            }
        }

        return countries;
    }

    @Override
    public Country getCountry(String countryCode) throws SslException, JSonExcepcion {
        RestCountriesClientImpl.LOGGER.debug("--> getCountry(" + countryCode + ")");

        ResponseEntity<String> response = null;

        try {
            final RestTemplate restTemplate = new RestTemplate(getRequestFactory());
            response = restTemplate.exchange(String.format(RestCountriesClient.URL_GET_COUNTRY_CODE, countryCode), HttpMethod.GET, null, String.class);
        } catch (final Exception exception) {
            RestCountriesClientImpl.LOGGER.debug("<-- getCountry(" + countryCode + ") SslException(" + exception.getMessage() + ")");
            throw new SslException(exception.getMessage());
        }

        Country country = null;
        if (response.getStatusCode() == HttpStatus.OK) {
            final ObjectMapper mapper = new ObjectMapper();

            // JSON from String to Object
            try {
                country = mapper.readValue(response.getBody(), Country.class);
            } catch (final IOException exception) {
                final JSonExcepcion exp = new JSonExcepcion(exception.getMessage());
                throw exp;
            }
        }

        RestCountriesClientImpl.LOGGER.debug(("<-- getCountry(" + countryCode + ") " + country) == null ? "NULL" : country.toString());
        return country;
    }

}
