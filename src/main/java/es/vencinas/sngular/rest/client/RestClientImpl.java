package es.vencinas.sngular.rest.client;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;

import es.vencinas.sngular.exception.SslException;

/**
 * The Class RestClientImpl.
 */
@Service
public class RestClientImpl implements RestClient {

    @Override
    public HttpComponentsClientHttpRequestFactory getRequestFactory() throws SslException {

        HttpComponentsClientHttpRequestFactory requestFactory = null;

        try {
            final TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = null;
            sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            final SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
            final CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException exception) {
            final SslException exp = new SslException(exception.getMessage());
            throw exp;
        }

        return requestFactory;
    }
}
