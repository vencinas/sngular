package es.vencinas.sngular.exception;

import javax.ws.rs.core.Response.Status.Family;

/**
 * The Class JSonExcepcion.
 */
public class JSonExcepcion extends SngularException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 977678620144157238L;

    /** The Constant STATUS_CODE. */
    private static final int STATUS_CODE = 16;

    /**
     * Instantiates a new j son excepcion.
     *
     * @param reasonPhrase
     *            the reason phrase
     */
    public JSonExcepcion(String reasonPhrase) {
        super(Family.OTHER, JSonExcepcion.STATUS_CODE, reasonPhrase);
    }

}
