package es.vencinas.sngular.exception;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;

import lombok.Data;

/**
 * Class used to provide custom StatusTypes, especially for the the Reason Phrase that appears in the HTTP Status Response.
 */
@Data
public abstract class SngularException extends Exception implements StatusType {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3391247511829542744L;

  /** The family. */
  private final Family family;

  /** The status code. */
  private final int statusCode;

  /** The reason phrase. */
  private final String reasonPhrase;

  /**
   * Instantiates a new sngular exception.
   *
   * @param family
   *            the family
   * @param statusCode
   *            the status code
   * @param reasonPhrase
   *            the reason phrase
   */
  public SngularException(final Family family, final int statusCode, final String reasonPhrase) {
    super();

    this.family = family;
    this.statusCode = statusCode;
    this.reasonPhrase = reasonPhrase;
  }

  /**
   * Instantiates a new sngular exception.
   *
   * @param status
   *            the status
   * @param reasonPhrase
   *            the reason phrase
   */
  protected SngularException(final Status status, final String reasonPhrase) {
    this(status.getFamily(), status.getStatusCode(), reasonPhrase);
  }

}
