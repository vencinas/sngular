package es.vencinas.sngular.rest.client;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.vencinas.sngular.domain.CurrencyConversion;
import es.vencinas.sngular.domain.Rates;
import es.vencinas.sngular.exception.SslException;
import es.vencinas.sngular.service.CurrencyRateFacadeImpl;

/**
 * The Class RestExchangeRatesClientImpl.
 */
@Service
public class RestExchangeRatesClientImpl extends RestClientImpl implements RestExchangeRatesClient {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyRateFacadeImpl.class);

    @Override
    public double getExcangeRates(String sourceCurrency, String targetCurrency) {
        Assert.notNull(sourceCurrency, "Código divisa obligatorio.");
        Assert.notNull(targetCurrency, "Código divisa obligatorio.");
        RestExchangeRatesClientImpl.LOGGER.info("[IN]\tRestExchangeRatesClientImpl:getExcangeRates(" + sourceCurrency + "," + targetCurrency + ")");

        if (sourceCurrency.equals(targetCurrency)) {
            RestExchangeRatesClientImpl.LOGGER.info("[OUT]\tRestExchangeRatesClientImpl:getExcangeRates()\t\t[OK] [1.0]");
            return 1.0d;
        }

        double exchangeRates = 0;

        ResponseEntity<String> response = null;
        try {
            final RestTemplate restTemplate = new RestTemplate(getRequestFactory());

            response = restTemplate.exchange(String.format(RestExchangeRatesClient.URL_GET_CURRENCY_COVERSION, sourceCurrency, targetCurrency), HttpMethod.GET, null, String.class);
        } catch (RestClientException | SslException exception1) {
            exception1.printStackTrace();
        }

        if (response.getStatusCode() == HttpStatus.OK) {
            CurrencyConversion currencyConversion = null;
            final ObjectMapper mapper = new ObjectMapper();

            // JSON from String to Object
            try {
                currencyConversion = mapper.readValue(response.getBody(), CurrencyConversion.class);

                final Rates rates = currencyConversion.getRates();
                final Map<String, Double> mapa = rates.getAdditionalProperties();
                for (final Map.Entry<String, Double> entry : mapa.entrySet())
                    exchangeRates = entry.getValue();
            } catch (final IOException exception) {
                RestExchangeRatesClientImpl.LOGGER.info("[OUT]\tRestExchangeRatesClientImpl:getExcangeRates()\t\t[ERROR] " + exception.getMessage());
                exchangeRates = -1;
            }
        }

        RestExchangeRatesClientImpl.LOGGER.info("[OUT]\tRestExchangeRatesClientImpl:getExcangeRates()\t\t[OK] [" + String.valueOf(exchangeRates) + "]");
        return exchangeRates;
    }

}
