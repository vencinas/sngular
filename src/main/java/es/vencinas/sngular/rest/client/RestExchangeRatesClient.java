package es.vencinas.sngular.rest.client;

/**
 * The Interface RestExchangeRatesClient.
 */
public interface RestExchangeRatesClient {

    /** The Constant URL_GET_CURRENCY_COVERSION. */
    static final String URL_GET_CURRENCY_COVERSION = "http://api.fixer.io/latest?base=%1s&symbols=%2s";

  /**
   * Gets the excange rates.
   *
   * @param sourceCurrency
   *            the source currency
   * @param targetCurrency
   *            the target currency
   * @return the excange rates
   */
  double getExcangeRates(String sourceCurrency, String targetCurrency);
}
