package es.vencinas.sngular.domain;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * Instantiates a new currency rate.
 */
@Data
public class CurrencyRate {

  /** The source country. */
  @NotNull
  @Size(max = 3)
  private String sourceCountry;

  /** The source currency. */
  @NotNull
  private String sourceCurrency;

  /** The target country. */
  @NotNull
  @Size(max = 3)
  private String targetCountry;

  /** The target currency. */
  @NotNull
  private String targetCurrency;

  /** The currency exchange rate. */
  private double currencyExchangeRate;

  /** The exchange date. */
  @Setter(value = AccessLevel.NONE)
  private LocalDateTime exchangeDate;

  /**
   * Gets the exchange date.
   *
   * @return the exchange date
   */
  public final LocalDateTime getExchangeDate() {
    return LocalDateTime.now();
  }
}
