package es.vencinas.sngular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SngularApplication {

  public static void main(String[] args) {
    SpringApplication.run(SngularApplication.class, args);
  }
}
