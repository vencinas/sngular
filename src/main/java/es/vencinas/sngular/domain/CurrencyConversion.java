package es.vencinas.sngular.domain;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({ "base", "date", "rates" })
public class CurrencyConversion {

  /** The base. */
  @JsonProperty("base")
  private String base;

  /** The date. */
  @JsonProperty("date")
  private String date;

  /** The rates. */
  @Valid
  @JsonProperty("rates")
  private Rates rates;

  /**
   * No args constructor for use in serialization.
   */
  public CurrencyConversion() {
  }

  /**
   * Instantiates a new currency conversion.
   *
   * @param base
   *            the base
   * @param date
   *            the date
   * @param rates
   *            the rates
   */
  public CurrencyConversion(String base, String date, Rates rates) {
    this.base = base;
    this.date = date;
    this.rates = rates;
  }

  /**
   * With base.
   *
   * @param base
   *            the base
   * @return the currency conversion
   */
  public CurrencyConversion withBase(String base) {
    this.base = base;
    return this;
  }

  /**
   * With date.
   *
   * @param date
   *            the date
   * @return the currency conversion
   */
  public CurrencyConversion withDate(String date) {
    this.date = date;
    return this;
  }

  /**
   * With rates.
   *
   * @param rates
   *            the rates
   * @return the currency conversion
   */
  public CurrencyConversion withRates(Rates rates) {
    this.rates = rates;
    return this;
  }
}
