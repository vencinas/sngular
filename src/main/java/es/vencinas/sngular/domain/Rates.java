package es.vencinas.sngular.domain;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Instantiates a new rates.
 */
@Data
public class Rates {

    /** The additional properties. */
    @JsonIgnore
    private final Map<String, Double> additionalProperties = new HashMap<>();

    /**
     * Gets the additional properties.
     *
     * @return the additional properties
     */
    @JsonAnyGetter
    public Map<String, Double> getAdditionalProperties() {
        return additionalProperties;
    }

    /**
     * Sets the additional property.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     */
    @JsonAnySetter
    public void setAdditionalProperty(String name, Double value) {
        additionalProperties.put(name, value);
    }

    /**
     * With additional property.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the rates
     */
    public Rates withAdditionalProperty(String name, Double value) {
        additionalProperties.put(name, value);
        return this;
    }
}
