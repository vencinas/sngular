No puedo considerar el proyecto como terminado, ya que faltan casi todas las pruebas unitarias, las cuales son imprescindibles para considerar que un projecto esté terminado. Hay pruebas unitarias del servicio rest que se presta.

Para arrancar el servicio rest, se utiliza Maven. La siguiente sentencia es la encargada de arrancar el servicio:

                                      mvn spring-boot:run


La interface del servicio rest es la siguiente:

1.- Se introduce un código de 3 letras del pais origen y un código o lista de códigos de 3 letras de los paises destino.
    Devuelve un JSON con el cambio de las divisas oficiales entre las modenas de los paises introduccidos.

	/api/currencyRate/v1
	/api/currencyRate/v1/{sourceCountry}/{targetCountry}

	Ejemplos:

	1.- Devuelve todos los paises con todos los cambios de divisa que tenga

	/api/currencyRate/v1

	Resultado: JSON muy grande. Tarda mucho tiempo en realizar la operación.

	2.- Un pais origen y un pais destino

	http://localhost:8080/api/currencyRate/v1/ESP/GB

	Resultado:
```
#!json
	{"GB":[{
	    "sourceCountry": "ESP",
	    "sourceCurrency": "EUR",
	    "targetCountry": "GB",
	    "targetCurrency": "GBP",
	    "currencyExchangeRate": 0.84475,
	    "exchangeDate": {
	        "dayOfMonth": 13,
	        "dayOfWeek": "TUESDAY",
	        "dayOfYear": 257,
	        "month": "SEPTEMBER",
	        "monthValue": 9,
	        "year": 2016,
	        "hour": 14,
	        "minute": 8,
	        "nano": 838000000,
	        "second": 29,
	        "chronology": {
	            "id": "ISO",
	            "calendarType": "iso8601"
	        }
	    }
	}]}
```

	3.- Un pais origen y una lista de paises destino (2 paises)

	http://localhost:8080/api/currencyRate/v1/ESP/GB,AFG

	Resultado:

```
#!json
	{"AFG": [{
	   "sourceCountry": "ESP",
	   "sourceCurrency": "EUR",
	   "targetCountry": "AFG",
	   "targetCurrency": "AFN",
	   "currencyExchangeRate": 0,
	   "exchangeDate": {
	       "dayOfMonth": 13,
	       "dayOfWeek": "TUESDAY",
	       "dayOfYear": 257,
	       "month": "SEPTEMBER",
	       "monthValue": 9,
	       "year": 2016,
	       "hour": 14,
	       "minute": 11,
	       "nano": 121000000,
	       "second": 16,
	       "chronology": {
	           "id": "ISO",
	           "calendarType": "iso8601"
	       }
	   }
	}],
	"GB": [{
	    "sourceCountry": "ESP",
	    "sourceCurrency": "EUR",
	    "targetCountry": "GB",
	    "targetCurrency": "GBP",
	    "currencyExchangeRate": 0.84475,
	    "exchangeDate": {
	        "dayOfMonth": 13,
	        "dayOfWeek": "TUESDAY",
	        "dayOfYear": 257,
	        "month": "SEPTEMBER",
	        "monthValue": 9,
	        "year": 2016,
	        "hour": 14,
	        "minute": 11,
	        "nano": 122000000,
	        "second": 16,
	        "chronology": {
	            "id": "ISO",
	            "calendarType": "iso8601"
	        }
	    }
	}]}
```
