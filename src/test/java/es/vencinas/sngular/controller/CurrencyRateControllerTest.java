package es.vencinas.sngular.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import es.vencinas.sngular.domain.CurrencyRate;
import es.vencinas.sngular.service.CurrencyRateFacade;
import es.vencinas.sngular.service.CurrencyRateFacadeImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class CurrencyRateControllerTest {

  private CurrencyRateFacade currencyRateFacadeMock;

  private CurrencyRateController currencyRateController;

  public CurrencyRateControllerTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
    currencyRateController = new CurrencyRateController();
    currencyRateFacadeMock = Mockito.mock(CurrencyRateFacadeImpl.class);
    ReflectionTestUtils.setField(currencyRateController, "currencyRateFacade", currencyRateFacadeMock);
  }

  @After
  public void tearDown() {

  }

  @Test
  public void getCurrencyRates() {
      String sourceCountry = "ESP";
      List<String> targetCountries = new ArrayList<>();
      targetCountries.add("USA");
      ResponseEntity<Map<String, List<CurrencyRate>>> result = currencyRateController.getCurrencyRates(sourceCountry, targetCountries);

      Assert.assertEquals(result.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void getCurrencyRatesList() {
      String sourceCountry = "ESP";
      List<String> targetCountries = new ArrayList<>();
      targetCountries.add("USA");
      targetCountries.add("GB");
      ResponseEntity<Map<String, List<CurrencyRate>>> result = currencyRateController.getCurrencyRates(sourceCountry, targetCountries);

      Assert.assertEquals(result.getStatusCode(), HttpStatus.OK);
  }
  

  @Test
  public void getAllCurrencyRates() {
      ResponseEntity<Map<String, List<CurrencyRate>>> result = currencyRateController.getAllCurrencyRates();

      Assert.assertEquals(result.getStatusCode(), HttpStatus.OK);
  }
  
}
