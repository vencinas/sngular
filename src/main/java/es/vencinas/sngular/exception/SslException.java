package es.vencinas.sngular.exception;

import javax.ws.rs.core.Response.Status.Family;

/**
 * The Class SslException.
 */
public class SslException extends SngularException {

    /** The Constant STATUS_CODE. */
    private static final int STATUS_CODE = 15;

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5347490955660070279L;

    /**
     * Instantiates a new ssl exception.
     *
     * @param reasonPhrase
     *            the reason phrase
     */
    public SslException(String reasonPhrase) {
        super(Family.OTHER, SslException.STATUS_CODE, reasonPhrase);
    }

}
