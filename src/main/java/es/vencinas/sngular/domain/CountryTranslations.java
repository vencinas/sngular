package es.vencinas.sngular.domain;

import lombok.Data;

/**
 * Instantiates a new country translations.
 */
@Data
public class CountryTranslations {

  /** The de. */
  private String de;
  
  /** The es. */
  private String es;
  
  /** The fr. */
  private String fr;
  
  /** The ja. */
  private String ja;
  
  /** The it. */
  private String it;

}
