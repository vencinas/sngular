package es.vencinas.sngular.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import es.vencinas.sngular.domain.Country;
import es.vencinas.sngular.domain.CurrencyRate;
import es.vencinas.sngular.exception.JSonExcepcion;
import es.vencinas.sngular.exception.NotFountSngularException;
import es.vencinas.sngular.exception.SslException;
import es.vencinas.sngular.rest.client.RestCountriesClient;
import es.vencinas.sngular.rest.client.RestExchangeRatesClient;

/**
 * The Class CurrencyRateFacadeImpl.
 */
@Service
public class CurrencyRateFacadeImpl implements CurrencyRateFacade {

    /** The Constant BASE. */
    private static final String BASE = "EUR";

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyRateFacadeImpl.class);

    /** The rest countries client. */
    @Autowired
    private RestCountriesClient restCountriesClient;

    /** The rest exchange rates client. */
    @Autowired
    private RestExchangeRatesClient restExchangeRatesClient;

    @Override
    public Map<String, List<CurrencyRate>> getCurrenciesRateCountry(String countryCodeSource, List<String> countryCodeTarget) throws NotFountSngularException {
        final Map<String, List<CurrencyRate>> result = new HashMap<>();

        for (final String country : countryCodeTarget) {
            final List<CurrencyRate> currencyRate = getCurrenciesRateCountry(countryCodeSource, country);
            result.put(country, currencyRate);
        }

        return result;
    }

    @Override
    public List<CurrencyRate> getCurrenciesRateCountry(String countryCodeSource, String countryCodeTarget) throws NotFountSngularException {
        Assert.notNull(countryCodeSource, "Source country required.");
        Assert.notNull(countryCodeTarget, "Target country required.");
        LOGGER.info("[IN]\tCurrencyRateFacadeImpl:findByCountryCode(" + countryCodeSource + "," + countryCodeTarget + ")");

        List<CurrencyRate> result = null;

        List<String> currenciesRateSourceCountry = null;
        try {
            final Country sourceCountry = restCountriesClient.getCountry(countryCodeSource);
            currenciesRateSourceCountry = sourceCountry == null ? null : sourceCountry.getCurrencies();
        } catch (JSonExcepcion | SslException exception) {
            LOGGER.error("Source country NOT FOUND");
            NotFountSngularException e = new NotFountSngularException("Source country not found.");
            throw e;
        }

        List<String> currenciesRateTargetCountry = null;
        try {
            final Country targetCountry = restCountriesClient.getCountry(countryCodeTarget);
            currenciesRateTargetCountry = targetCountry == null ? null : targetCountry.getCurrencies();
        } catch (JSonExcepcion | SslException exception) {
            LOGGER.error("Target country NOT FOUND");
            NotFountSngularException e = new NotFountSngularException("Target country not found.");
            throw e;
        }

        if (currenciesRateSourceCountry != null && currenciesRateTargetCountry != null) {
            result = new ArrayList<>();

            for (final String currencyRateSource : currenciesRateSourceCountry) {
                for (final String currencyRateTarget : currenciesRateTargetCountry) {

                    final CurrencyRate currencyRate = findExchangeRate(countryCodeSource, currencyRateSource, countryCodeTarget, currencyRateTarget);

                    result.add(currencyRate);
                }
            }
        }

        LOGGER.info("[OUT]\tCurrencyRateFacadeImpl:findByCountryCode()\t\t[OK]");
        return result;
    }

    /**
     * Find exchange rate using Service Rest ExchangeRates.
     *
     * @param countryCodeSource
     *            the country code source
     * @param currencyRateSource
     *            the currency rate source
     * @param countryCodeTarget
     *            the country code target
     * @param currencyRateTarget
     *            the currency rate target
     * @return the currency rate
     */
    private CurrencyRate findExchangeRate(String countryCodeSource, String currencyRateSource, String countryCodeTarget, String currencyRateTarget) {
        final CurrencyRate currencyRate = new CurrencyRate();

        currencyRate.setSourceCountry(countryCodeSource);
        currencyRate.setSourceCurrency(currencyRateSource);

        currencyRate.setTargetCountry(countryCodeTarget);
        currencyRate.setTargetCurrency(currencyRateTarget);
        final double currencyConversion = restExchangeRatesClient.getExcangeRates(currencyRateSource, currencyRateTarget);
        currencyRate.setCurrencyExchangeRate(currencyConversion);

        return currencyRate;
    }

    @Override
    public Map<String, List<CurrencyRate>> getAllCurrencyRates() {
        LOGGER.info("[IN]\tCurrencyRateFacadeImpl:getAllCurrencyRates()");

        final Map<String, List<CurrencyRate>> result = new HashMap<>();

        try {
            final List<Country> listCountries = restCountriesClient.getAllCountries();

            for (final Country country : listCountries) {
                final List<String> currenciesRateCountry = country.getCurrencies();

                List<CurrencyRate> lstCurrencyRate = new ArrayList<CurrencyRate>();
                for (final String currenciesRate : currenciesRateCountry) {
                    final CurrencyRate currencyRate = findExchangeRate(country.getAlpha3Code(), BASE, "", currenciesRate);
                    lstCurrencyRate.add(currencyRate);
                }
                result.put(country.getAlpha3Code(), lstCurrencyRate);
            }
        } catch (final SslException exception) {
            exception.printStackTrace();
        }

        LOGGER.info("[OUT]\tCurrencyRateFacadeImpl:getAllCurrencyRates()\t\t[OK]");
        return result;
    }
}
