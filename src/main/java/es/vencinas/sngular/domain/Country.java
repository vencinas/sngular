package es.vencinas.sngular.domain;

import java.util.List;

import lombok.Data;

/**
 * Instantiates a new country.
 */
@Data
public class Country {

  /** The name. */
  private String name;

  /** The top level domain. */
  private List<String> topLevelDomain;

  /** The alpha 2 code. */
  private String alpha2Code;

  /** The alpha 3 code. */
  private String alpha3Code;

  /** The currencies. */
  private List<String> currencies;

  /** The calling codes. */
  private List<String> callingCodes;

  /** The capital. */
  private String capital;

  /** The alt spellings. */
  private List<String> altSpellings;

  /** The relevance. */
  private String relevance;

  /** The region. */
  private String region;

  /** The subregion. */
  private String subregion;

  /** The languages. */
  private List<String> languages;

  /** The numeric code. */
  private String numericCode;

  /** The translations. */
  private CountryTranslations translations;

  /** The population. */
  private Integer population;

  /** The latlng. */
  private List<Double> latlng;

  /** The demonym. */
  private String demonym;

  /** The area. */
  private Double area;

  /** The gini. */
  private Double gini;

  /** The timezones. */
  private List<String> timezones;

  /** The borders. */
  private List<String> borders;

  /** The native name. */
  private String nativeName;
}
