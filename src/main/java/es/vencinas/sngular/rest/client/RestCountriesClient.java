package es.vencinas.sngular.rest.client;

import java.util.List;

import es.vencinas.sngular.domain.Country;
import es.vencinas.sngular.exception.JSonExcepcion;
import es.vencinas.sngular.exception.SslException;

/**
 * The Interface RestCountriesClient.
 */
public interface RestCountriesClient {

    /** The Constant URL_GET_ALL_COUNTRIES. */
    static final String URL_GET_ALL_COUNTRIES = "https://restcountries.eu/rest/v1/all";

    /** The Constant URL_GET_COUNTRY_CODE. */
    static final String URL_GET_COUNTRY_CODE = "https://restcountries.eu/rest/v1/alpha/%s";

  /**
   * Gets the all countries.
   *
   * @return the all countries
   * @throws SslException
   *             the ssl exception
   */
  List<Country> getAllCountries() throws SslException;

  /**
   * Gets the country.
   *
   * @param country
   *            the country
   * @return the country
   * @throws SslException
   *             the ssl exception
   * @throws JSonExcepcion
   *             the j son excepcion
   */
  Country getCountry(String country) throws SslException, JSonExcepcion;
}
