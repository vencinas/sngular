package es.vencinas.sngular.service;

import java.util.List;
import java.util.Map;

import es.vencinas.sngular.domain.CurrencyRate;
import es.vencinas.sngular.exception.NotFountSngularException;

/**
 * Service for managing currency rate.
 *
 * @author Valentín Encinas
 */
public interface CurrencyRateFacade {

  /**
   * Gets the currencies rate country.
   *
   * @param countryAlfaCode3Source the country code source
   * @param countryAlfaCode3Target the country code target
   * @return the currencies rate country
   */
  List<CurrencyRate> getCurrenciesRateCountry(String countryAlfaCode3Source, String countryAlfaCode3Target) throws NotFountSngularException;

  /**
   * Gets the currencies rate country.
   *
   * @param countryAlfaCode3Source the country code source
   * @param lstCountryAlfaCode3Target the list countries code target
   * @return the currencies rate country
   */
  Map<String, List<CurrencyRate>> getCurrenciesRateCountry(String countryAlfaCode3Source, List<String> lstCountryAlfaCode3Target) throws NotFountSngularException;

  /**
   * Gets the all currency rates.
   *
   * @return the all currency rates
   */
  Map<String, List<CurrencyRate>>  getAllCurrencyRates();

}
